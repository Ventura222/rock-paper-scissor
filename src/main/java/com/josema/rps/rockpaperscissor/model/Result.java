package com.josema.rps.rockpaperscissor.model;

public enum Result {

    PLAYER1_WIN, PLAYER2_WIN, TIE

}
