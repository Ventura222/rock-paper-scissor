import React, {createContext} from 'react';
import {Route, NavLink, HashRouter} from "react-router-dom";
import Home from "./Home";
import Total from "./Total";
import {usePlay} from "./Hooks";

export const MatchesContext = createContext(null);

const App = () => {
    return (
      <div className="App">
          <HashRouter>
          <nav className="navbar navbar-default">
              <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">Rock, Paper, Scissor</a>
                    </div>
                  <ul className="nav navbar-nav">
                      <li><NavLink exact to="/">Home</NavLink></li>
                      <li><NavLink to="/total">Total</NavLink></li>
                  </ul>
              </div>
          </nav>
          <div className="Content">
              <MatchesContext.Provider value={usePlay()}>
              <Route exact path="/" component={Home}/>
              <Route path="/total" component={Total}/>
              </MatchesContext.Provider>
          </div>
          </HashRouter>
      </div>
    );
};

export default App;
