package com.josema.rps.rockpaperscissor.model;

import lombok.Getter;

@Getter
public class Total {
    private int rounds;
    private int player1Wins;
    private int player2Wins;
    private int draws;

    public void addRound(){
        rounds ++;
    }
    public void addPlayer1Wins(){
        player1Wins++;
    }

    public void addPlayer2Wins(){
        player2Wins++;
    }

    public void addDraws(){
        draws++;
    }

    public void reset(){
        rounds = 0;
        player1Wins = 0;
        player2Wins = 0;
        draws = 0;
    }
}
