# To run the project

1. Run the spring app through intellij IDE
2. Go to src/main/app in the terminal and execute "npm start"

After "npm start" it should open the app automatically in a browser with localhost:3000  
   
Note: Backend and frontend must be running in parallel. 