package com.josema.rps.rockpaperscissor.controller;

import com.josema.rps.rockpaperscissor.model.Chose;
import com.josema.rps.rockpaperscissor.model.Match;
import com.josema.rps.rockpaperscissor.model.Result;
import com.josema.rps.rockpaperscissor.model.Total;
import com.josema.rps.rockpaperscissor.service.GameService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest
public class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    GameService gameService;

    @Test
    public void playGame() throws Exception {

        when(gameService.playGame()).thenReturn(new Match(Chose.ROCK, Chose.SCISSOR, Result.PLAYER1_WIN));

        this.mockMvc.perform(post("/play").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.player1Chose").value(Chose.ROCK.toString()))
                .andExpect(jsonPath("$.player2Chose").value(Chose.SCISSOR.toString()))
                .andExpect(jsonPath("$.result").value(Result.PLAYER1_WIN.toString()));
    }


    @Test
    public void getTotal() throws Exception {
        when(gameService.getTotal()).thenReturn(new Total());

        this.mockMvc.perform(get("/total"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.rounds").value(0))
                .andExpect(jsonPath("$.player1Wins").value(0))
                .andExpect(jsonPath("$.player2Wins").value(0))
                .andExpect(jsonPath("$.draws").value(0));
    }


}
