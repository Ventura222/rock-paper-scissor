import React from "react";
import {useTotal} from "./Hooks";

const Total = () => {

    const {rounds, player1Wins, player2Wins, draws} = useTotal();

        return (
            <div className="container">
                <ul className="list-group">
                    <li className="list-group-item">Totals rounds: {rounds}</li>
                    <li className="list-group-item">Totals wins for 1st player: {player1Wins}</li>
                    <li className="list-group-item">Totals wins for 2nd player: {player2Wins}</li>
                    <li className="list-group-item">Total Draws: {draws}</li>
                </ul>
            </div>
        );
};

export default Total;