package com.josema.rps.rockpaperscissor.model;

import java.util.Random;

public enum Chose {

    PAPER, ROCK, SCISSOR;

    private static Random random = new Random();
    private static final Chose[] VALUES = values();

    public static Chose randomChose() {
        return VALUES[random.nextInt(VALUES.length )];
    }
}
