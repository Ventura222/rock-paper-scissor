package com.josema.rps.rockpaperscissor.controller;

import com.josema.rps.rockpaperscissor.model.Match;
import com.josema.rps.rockpaperscissor.model.Total;
import com.josema.rps.rockpaperscissor.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
public class GameController {

    private Logger logger = LoggerFactory.getLogger(GameController.class.getName());

    @Autowired
    GameService gameService;


    @PostMapping("/play")
    ResponseEntity<?> playGame() {
        logger.debug("Play Game request");
        Match match = gameService.playGame();
        logger.debug("Result: {}", match);

        return ResponseEntity.ok(match);
    }

    @GetMapping("/total")
    Total retrieveTotal(){
        logger.debug("Retrieve total request");
        return gameService.getTotal();
    }

}
