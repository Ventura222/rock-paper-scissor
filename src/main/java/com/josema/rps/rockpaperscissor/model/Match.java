package com.josema.rps.rockpaperscissor.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Match {
    private Chose player1Chose;
    private Chose player2Chose;
    private Result result;
}
