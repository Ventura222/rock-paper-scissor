import {MatchesContext} from "./App";
import React, {useContext} from "react";

const MatchItem = ({match, index}) =>{
    return (<tr>
        <th scope="row"> {index} </th>
        <td> {match.player1Chose} </td>
        <td> {match.player2Chose} </td>
        <td> {match.result} </td>
    </tr>)
};


const MatchList = () => {

    const {matches, play, reset} = useContext(MatchesContext);

    return (
        <div className="container">

            <p> Rounds played {matches.length} </p>

            {matches.length > 0 &&
            <table className="table table-striped">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Player 1 Chose</th>
                    <th scope="col">Player 2 Chose</th>
                    <th scope="col">Result</th>
                </tr>
                </thead>
                <tbody>
                {matches.map((match, i) => {
                    return <MatchItem match={match} key={i} index={i}/>
                })}
                </tbody>
            </table>
            }

            <div className="footer">
                <button type="button" className="btn btn-primary" onClick={play}>Play</button>
                <button type="button" className="btn btn-secondary" onClick={reset}>Reset</button>

            </div>
        </div>
    )
};

export default MatchList;