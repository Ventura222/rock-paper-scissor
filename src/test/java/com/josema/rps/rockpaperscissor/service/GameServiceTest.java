package com.josema.rps.rockpaperscissor.service;

import com.josema.rps.rockpaperscissor.model.Chose;
import com.josema.rps.rockpaperscissor.model.Match;
import com.josema.rps.rockpaperscissor.model.Result;
import com.josema.rps.rockpaperscissor.model.Total;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

class GameServiceTest {

    private GameService gameService = new GameService();


    @Test
    void checkTotalAndEvaluateCall(){
        GameService spyGameService = Mockito.spy(new GameService());
        spyGameService.playGame();
        verify(spyGameService).evaluate(any(),any());
        verify(spyGameService).updateTotal(any());
    }

    @Test
    void updateTotal_Test(){
        Total total = gameService.getTotal();
        assertEquals(0,total.getRounds());
        assertEquals(0,total.getDraws());
        assertEquals(0,total.getPlayer2Wins());
        assertEquals(0,total.getPlayer1Wins());

        gameService.updateTotal(Result.PLAYER1_WIN);
        assertEquals(1, total.getPlayer1Wins());

        gameService.updateTotal(Result.PLAYER2_WIN);
        assertEquals(1, total.getPlayer2Wins());

        gameService.updateTotal(Result.TIE);
        assertEquals(1,total.getDraws());

        assertEquals(3, total.getRounds());
    }

    @Test
    void Player2ChooseAlwaysRock_Test(){
        Match match = gameService.playGame();
        assertEquals(Chose.ROCK, match.getPlayer2Chose());
    }

    @Test
    void Player1IsNotNull_Test() {
        Match match = gameService.playGame();
        assertNotNull(match.getPlayer1Chose());
    }

    @Test
    void resultIsValid_Test(){
        Match match = gameService.playGame();
        assertEquals(gameService.evaluate(match.getPlayer1Chose(), match.getPlayer2Chose()), match.getResult());
    }

    @Test
    void tie_Test(){
        Result result = gameService.evaluate(Chose.ROCK, Chose.ROCK);
        assertEquals(Result.TIE, result);

        result = gameService.evaluate(Chose.PAPER, Chose.PAPER);
        assertEquals(Result.TIE, result);

        result = gameService.evaluate(Chose.SCISSOR, Chose.SCISSOR);
        assertEquals(Result.TIE, result);
    }

    @Test
    void player1Win_Test(){
        Result result = gameService.evaluate(Chose.ROCK, Chose.SCISSOR);
        assertEquals(Result.PLAYER1_WIN, result);

        result = gameService.evaluate(Chose.SCISSOR, Chose.PAPER);
        assertEquals(Result.PLAYER1_WIN, result);

        result = gameService.evaluate(Chose.PAPER, Chose.ROCK);
        assertEquals(Result.PLAYER1_WIN, result);

    }

    @Test
    void player2Win_Test(){

        Result result = gameService.evaluate(Chose.ROCK, Chose.PAPER);
        assertEquals(Result.PLAYER2_WIN, result);

        result = gameService.evaluate(Chose.PAPER, Chose.SCISSOR);
        assertEquals(Result.PLAYER2_WIN, result);

        result = gameService.evaluate(Chose.SCISSOR, Chose.ROCK);
        assertEquals(Result.PLAYER2_WIN, result);

    }

}
