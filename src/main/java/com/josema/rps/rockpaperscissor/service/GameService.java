package com.josema.rps.rockpaperscissor.service;

import com.josema.rps.rockpaperscissor.model.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class GameService {

    private Player player1 = Chose::randomChose;
    private Player player2 = () -> Chose.ROCK;

    /**
     * WinMap maps the chose that wins to another
     * Example: Rock wins Scissor. winMap[Rock] -> Scissor
     */
    private Map<Chose,Chose> winMap = new HashMap<>();

    private Total total = new Total();

    public GameService() {
        winMap.put(Chose.ROCK, Chose.SCISSOR);
        winMap.put(Chose.SCISSOR, Chose.PAPER);
        winMap.put(Chose.PAPER, Chose.ROCK);
    }

    public Match playGame() {

        Chose player1Chose = player1.play();
        Chose player2Chose = player2.play();

        Result result = evaluate(player1Chose, player2Chose);

        updateTotal(result);

        return new Match(player1Chose, player2Chose, result);
    }

    void updateTotal(Result result) {
        switch (result){
            case PLAYER1_WIN: total.addPlayer1Wins(); break;
            case PLAYER2_WIN: total.addPlayer2Wins(); break;
            case TIE: total.addDraws(); break;
        }

        total.addRound();
    }

    Result evaluate(Chose player1Chose, Chose player2Chose) {

        if(player1Chose.equals(player2Chose)) {
            return Result.TIE;
        } else if (winMap.get(player1Chose).equals(player2Chose)) {
            return Result.PLAYER1_WIN;
        } else {
            return Result.PLAYER2_WIN;
        }

    }

    public Total getTotal() {
        return total;
    }
}
