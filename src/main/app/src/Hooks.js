import {useEffect, useState} from "react";

export const usePlay = () => {
    const [matches, setMatches] = useState([]);
    const play = () => {
        let playURL = new URL("/play", process.env.REACT_APP_API_URL);
        let conf = {
          method: 'POST',
            mode: 'cors'
        };
        fetch(playURL.toString(), conf)
            .then(response => response.json())
            .then((newMatch) => {
            console.log(newMatch);
            let newMatches = matches.slice(0);
            newMatches.push(newMatch)
            setMatches(newMatches);
        } )
    };

    const reset = () => {
        setMatches([]);
    };

    return {
        matches,
        play,
        reset
    };

};

export const useTotal = () => {
    let totalURL = new URL("/total", process.env.REACT_APP_API_URL);
    let conf = {
        mode: 'cors'
    };

    const [total, setTotal] = useState([]);
    useEffect(() => {
        fetch(totalURL.toString(), conf)
            .then(response => response.json())
            .then((newTotal) => {
                console.log(newTotal);
                setTotal(newTotal);
                })
    } ,[]);

    return total;

};